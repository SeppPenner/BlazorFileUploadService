﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReadRequest.cs" company="Haemmer Electronics">
//   Copyright (c) 2019 All rights reserved.
// </copyright>
// <summary>
//   This struct contains read request data for the <see cref="SharedMemoryFileListEntryStream"/>.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace BlazorInputFile
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// This struct contains read request data for the <see cref="SharedMemoryFileListEntryStream"/>.
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct ReadRequest
    {
        /// <summary>
        /// The input file element reference identifier.
        /// </summary>
        [FieldOffset(0)]
        public string InputFileElementReferenceId;

        /// <summary>
        /// The file identifier.
        /// </summary>
        [FieldOffset(4)]
        public int FileId;

        /// <summary>
        /// The source offset.
        /// </summary>
        [FieldOffset(8)]
        public long SourceOffset;

        /// <summary>
        /// The destination.
        /// </summary>
        [FieldOffset(16)]
        public byte[] Destination;

        /// <summary>
        /// The destination offset.
        /// </summary>
        [FieldOffset(20)]
        public int DestinationOffset;

        /// <summary>
        /// The maximum number of bytes.
        /// </summary>
        [FieldOffset(24)]
        public int MaximumBytes;
    }
}
