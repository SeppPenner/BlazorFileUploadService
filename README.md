BlazorFileUploadService
====================================

BlazorFileUploadService is a project to support simple file uploads using [Blazor](https://github.com/aspnet/Blazor).
The project was written and tested in .NetCore 3.0.

[![Build status](https://ci.appveyor.com/api/projects/status/2v9xurld6yn1s5aa?svg=true)](https://ci.appveyor.com/project/SeppPenner/blazorfileuploadservice)
[![GitHub issues](https://img.shields.io/github/issues/SeppPenner/BlazorFileUploadService.svg)](https://github.com/SeppPenner/BlazorFileUploadService/issues)
[![GitHub forks](https://img.shields.io/github/forks/SeppPenner/BlazorFileUploadService.svg)](https://github.com/SeppPenner/BlazorFileUploadService/network)
[![GitHub stars](https://img.shields.io/github/stars/SeppPenner/BlazorFileUploadService.svg)](https://github.com/SeppPenner/BlazorFileUploadService/stargazers)
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://raw.githubusercontent.com/SeppPenner/BlazorFileUploadService/master/License.txt)
[![Known Vulnerabilities](https://snyk.io/test/github/SeppPenner/BlazorFileUploadService/badge.svg)](https://snyk.io/test/github/SeppPenner/BlazorFileUploadService)

## Inspired by
* https://bayfiles.com/
* https://anonfiles.com/
* https://openload.cc/
* https://letsupload.cc/
* https://megaupload.nz/
* https://share-online.is/
* https://rapidshare.nu/

Change history
--------------

* **Version 1.0.0.0 (2019-xx-xx)** : 1.0 release.
* **Version 1.0.0.0 (2019-06-21)** : Created project.